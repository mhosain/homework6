






/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework6;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
//import xml_utilities.XMLUtilities;

/**
 *
 * @author Shafi
 */
public class Homework6 extends Application {
  static ArrayList<Reg> reg;
    static ArrayList<Reg> mol; 
    
    static DocumentBuilderFactory docBuilderFactory;
     static DocumentBuilder docBuilder;
     static Document doc;
     static Document doc2;
     static Document doc3;
    Label name;
    Label capital;
    Label districts;
    Label fGuess;
    Label current;
    private ImageView clickedContinent;
    Button btn2;
    Button btn3;
    Button help;
    Button settings;
    private static StringBuilder builder;
    private File worldSchema;
    private Scene scene;
    private Scene scene2;
    private StackPane root;
    private BorderPane windowPane;
    ImageView imageView;
    private ImageView iclickedCountry;
    //private XMLUtilities xmlUtil;
    public TreeMap<String, ImageView> guiImages = new TreeMap<String,ImageView>();
    public static final String GUI_PATH = "./data/gui/";
    public static final String BACKGROUND_FILE_PATH = GUI_PATH + "RegioVincoBackground.jpg";
    public static  int BACKGROUND_X = 500;
    public static  int BACKGROUND_Y = 500;
    public static final String TITLE_TYPE = "TITLE_TYPE";
    public static final int TITLE_X = 900;
    public static final int TITLE_Y = 0;
    public static final String TITLE_FILE_PATH = GUI_PATH + "RegioVincoTitle.png";
     public static final String MAPS_PATH = "./data/maps/";
     public static final String clickedContinent_MAP_FILE_PATH = MAPS_PATH+"Europe Map.png";
    public static final String MOLDOVA_MAP_FILE_PATH = MAPS_PATH + "Moldova Map.png";
    public static final String WORLD_MAP_FILE_PATH = MAPS_PATH + "The World Map.png";
    private WritableImage mapImage;
    private PixelReader mapPixelReader;
    private PixelWriter mapPixelWriter;
    private HashMap<Color, String> cont;
    private static HashMap<Color, String> cr;
    private static HashMap<Color, String> cc;
    private static HashMap<Color, String> cmr;
    private static HashMap<Color, String> cmc;
    Image link1;
    
    
    private HashMap <String ,String> cac;
    
    private static HashMap<String, Color> rc;
    public static final String WORLD_ID = "World";
    public static final String WORLD_REGIONS_NODE = "world_regions";
    public static final String REGIONS_LIST_NODE = "regions_list";
    public static final String REGIONS_MAPPINGS_NODE = "regions_mappings";
    public static final String REGION_NODE = "region";
    public static final String SUB_REGION_NODE = "sub_region";
    public static final String ID_ATTRIBUTE = "id";
    public static final String NAME_ATTRIBUTE = "name";
    public static final String TYPE_ATTRIBUTE = "type";
    public static final String CAPITAL_ATTRIBUTE = "capital";
    private int colorReader;

    // FOR NICELY FORMATTED XML OUTPUT
    public static final String XML_INDENT_PROPERTY = "{http://xml.apache.org/xslt}indent-amount";
    public static final String XML_INDENT_VALUE = "5";
    public static final String YES_VALUE = "yes";
    
    @Override
    public void start(Stage primaryStage) {
        
        Button btn = new Button();
        btn.setText("Enter the world ");
        btn.setScaleX(2);
        btn.setScaleY(2);
        
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
               root.getChildren().remove(0, 3);
               
               
              
               link1 = loadImage(WORLD_MAP_FILE_PATH);
               Image image = loadImage(WORLD_MAP_FILE_PATH);
               
               imageView = new ImageView();
        imageView.setImage(image);
        
       
        // Obtain PixelReader
        PixelReader pixelReader = image.getPixelReader();
//        System.out.println("Image Width: "+image.getWidth());
//        System.out.println("Image Height: "+image.getHeight());
//        System.out.println("Pixel Format: "+pixelReader.getPixelFormat());
//        
        // Create WritableImage
         WritableImage wImage = new WritableImage(
                 (int)image.getWidth(),
                 (int)image.getHeight());
         PixelWriter pixelWriter = wImage.getPixelWriter();
       int c =0;
        // Determine the color of each pixel in a specified row
        for(int readY=0;readY<image.getHeight();readY++){
            for(int readX=0; readX<image.getWidth();readX++){
                Color color = pixelReader.getColor(readX,readY);
//                System.out.println("\nPixel color at coordinates ("+
//                        readX+","+readY+") "
//                        +color.toString());
//                System.out.println("R = "+color.getRed());
//                System.out.println("G = "+color.getGreen());
//                System.out.println("B = "+color.getBlue());
//                System.out.println("Opacity = "+color.getOpacity());
//                System.out.println("Saturation = "+color.getSaturation());
                
                // Now write a brighter color to the PixelWriter.
                if (color.equals(Color.rgb(150, 150, 150) )){
                    pixelWriter.setColor(readX,readY,color);
                }
                else if (color.equals(Color.rgb(0, 162, 232) )){
                    pixelWriter.setColor(readX,readY,color);
                }
                else if (color.equals(Color.rgb(220, 110, 0) )){
                    pixelWriter.setColor(readX,readY,Color.BLACK);
                }
                        
                
                
               else
                { pixelWriter.setColor(readX,readY,Color.GREY);}
               
            }
            
        }
//               ImageView mapView = guiImages.get("worldMap");
//               Image start = mapView.getImage();
//              
//               int w = (int)start.getWidth();
//               int h = (int)start.getHeight();
//              
//               mapImage = new WritableImage( mapPixelReader,w,h);
//               mapView.setImage(mapImage);
//              mapPixelReader = start.getPixelReader();
//              for (int i = 0;i<mapImage.getHeight();i++){
//        for(int j= 0;j<mapImage.getWidth();j++){
//            if((mapImage.getPixelReader().getColor(j, i)).equals(Color.rgb(150, 150, 150))){
//            mapImage.getPixelWriter().setColor(j, i, Color.BLACK);
//            }
//        }}
             
              
               
               
               
              // root.getChildren().get(0).translateXProperty().set(-300);
               //root.getChildren().get(0).translateXProperty().set(-50);
               
               
                //Scene scene2 = new Scene(root, 900, 700);
               //scene = new Scene (root,900,700);
        
               imageView.setImage(wImage);
        //StackPane root = new StackPane();
        root.getChildren().add(imageView);
        current = new Label();
        current.translateXProperty().setValue(500);
        current.translateYProperty().setValue(0);
        current.setText("Current Region ;");
        current.setStyle("-fx-text-fill: rgb(8, 4, 80);" +
"    -fx-background-color: rgb(168, 168,168);"+
      "-fx-padding: 0;"      +"-fx-font-size: 16pt ;"    );
        root.getChildren().add(current);
        current.setVisible(true);
        capital = new Label();
        capital.translateXProperty().setValue(500);
        capital.translateYProperty().setValue(100);
        capital.setText("Current Region ;");
        capital.setStyle("-fx-text-fill: rgb(8, 4, 80);" +
"    -fx-background-color: rgb(168, 168,168);"+
      "-fx-padding: 0;"      +"-fx-font-size: 16pt ;"    );
        
        
        
        
       // Scene scene = new Scene(root, 900, 700);
        
        primaryStage.setTitle("Continents of the world");
        primaryStage.setScene(scene);
        primaryStage.show();
        imageView.setOnMouseMoved((e -> {
            mapPixelReader =imageView.getImage().getPixelReader();
            Color  colorClicked =mapPixelReader.getColor((int)e.getX(), (int)e.getY());
            if (colorClicked.equals(Color.rgb(150, 150, 150) )){
                    current.setText("Current Region :Europe");
                }
            else {
            current.setText("Not playable region");}
            
        
        
        
        }));
//        doc.getDocumentElement ().normalize ();
//        System.out.println ("Root element of the doc is " + doc.getDocumentElement().getNodeName());
//         NodeList listOfContinents = doc.getElementsByTagName("sub_region");
//        int total = listOfContinents.getLength();
//        System.out.println("Total no of continent : " + total);     
               
               
               
               
               imageView.setOnMousePressed(e -> {
////                   
                   Color map = imageView.getImage().getPixelReader().getColor((int)e.getX(),(int) e.getY());
////                   if (colorOfclickedContinent==map){
                  mapPixelReader =imageView.getImage().getPixelReader();
                  mapImage  = new WritableImage(
                 (int)imageView.getImage().getWidth(),
                 (int)imageView.getImage().getHeight());
          mapPixelWriter = mapImage.getPixelWriter();
                 Color  colorClicked =mapPixelReader.getColor((int)e.getX(), (int)e.getY());
                 


           if (colorClicked.equals(Color.rgb(150,150, 150) )){
                    root.getChildren().remove(imageView);
                    root.getChildren().remove(current);
                    
        root.getChildren().add(capital);
        capital.setVisible(true);
        
                   clickedContinent = addGUIImage(root, "clickedContinent", loadImage(clickedContinent_MAP_FILE_PATH), 0, 0);
                   
                   root.getChildren().add(current);
                    btn2 = new Button();
        ImageView ilink1 = new ImageView(loadImage(WORLD_MAP_FILE_PATH));
//        ilink1.scaleXProperty().setValue(.25);
//        ilink1.scaleYProperty().setValue(.25);
        ilink1.setFitHeight(50);
        ilink1.setFitWidth(50);
        
        btn2.setGraphic(ilink1);
        btn2.setText(" Go back to world");
        btn2.translateYProperty().setValue(300);
        btn2.translateXProperty().setValue(-300);
        btn.setScaleX(2);
        btn.setScaleY(2);
         
                   root.getChildren().add(btn2);
                   clickedContinent.setOnMouseClicked ((a -> {
                 mapPixelReader =clickedContinent.getImage().getPixelReader();
               Color  cclickedCountry =mapPixelReader.getColor((int)a.getX(), (int)a.getY());  
              if (cclickedCountry.equals(Color.rgb(170, 170, 170) )){
                   root.getChildren().remove(current);
                    root.getChildren().remove(capital);
                    root.getChildren().remove(btn2);
                     root.getChildren().remove(clickedContinent);
                   
                    
                    
                   iclickedCountry = addGUIImage(root, "Moldova", loadImage(MOLDOVA_MAP_FILE_PATH), 0, 0);
                    root.getChildren().add(current);
                   root.getChildren().add(capital);
                   help = new Button();
                settings = new Button();
                 help.translateXProperty().setValue(500);
                help.setText("Help");
                settings.setText("Settings");
                help.translateYProperty().setValue(-250);
                settings.translateXProperty().setValue(500);
                settings.translateYProperty().setValue(-200);
                root.getChildren().add(help);
                root.getChildren().add(settings);
                 btn3 = new Button();
        ImageView link2 = new ImageView(loadImage(clickedContinent_MAP_FILE_PATH));
        link2.setFitHeight(50);
        link2.setFitWidth(50);
        btn3.setGraphic(link2);
        
        btn3.setText("Go back to continents");
        btn3.translateYProperty().setValue(300);
        root.getChildren().add(btn3);
               
                
                       
                help.setOnMouseClicked((g -> {
           root.getChildren().remove(current);
            root.getChildren().remove(capital);
           root.getChildren().remove(iclickedCountry);
           root.getChildren().remove(btn3);
            
           
            
        }));
                settings.setOnMouseClicked((g -> {
           root.getChildren().remove(current);
            root.getChildren().remove(capital);
           root.getChildren().remove(iclickedCountry);
           root.getChildren().remove(btn3);
            
           
            
        }));
                   
                    iclickedCountry.setOnMouseMoved((m -> {
            mapPixelReader =iclickedCountry.getImage().getPixelReader();
            Color  color =mapPixelReader.getColor((int)m.getX(), (int)m.getY());
            String currenter = cmr.get(color);
            current.setText( currenter);
       
        String cap = cmc.get(color);
        capital.setText(cap);
       
        
        btn3.setOnMouseClicked    ((g -> {
           root.getChildren().remove(current);
            root.getChildren().remove(capital);
           root.getChildren().remove(iclickedCountry);
           root.getChildren().remove(btn3);
            root.getChildren().add(clickedContinent);
           root.getChildren().add(current);
            root.getChildren().add(capital);
            root.getChildren().add(btn2);
           
            
        }));
       
      
        })); 
                }
                   
                   
                   }));
                  clickedContinent.setOnMouseMoved((f -> {
            mapPixelReader =clickedContinent.getImage().getPixelReader();
            Color  color =mapPixelReader.getColor((int)f.getX(), (int)f.getY());
            String currenter = cr.get(color);
            current.setText( currenter);
       
        String cap = cc.get(color);
        capital.setText(cap);
        btn2.setOnMouseClicked    ((g -> {
           
           rEuro ();
           root.getChildren().add(imageView);
            root.getChildren().add(current);
           
            
        }));
        })); 
                 
                }     
                 
                 
	});
               
//               
            }
        });
//        windowPane = new BorderPane();
//        windowPane.getChildren().add(btn);
//        windowPane.setVisible(true);
//       Pane test = new Pane();
//       test.getChildren().add(btn);
       btn.translateYProperty().setValue(0);
       btn.translateXProperty().setValue(0);
         root = new StackPane();
         
        
        
        
         
         addGUIImage(root, "firstImage", loadImage(BACKGROUND_FILE_PATH), BACKGROUND_X, BACKGROUND_Y);
         addGUIImage(root, TITLE_TYPE, loadImage(TITLE_FILE_PATH), TITLE_X, TITLE_Y);
         root.getChildren().add(btn);
        root.getChildren().get(1).translateXProperty().setValue(300);
        root.getChildren().get(1).translateYProperty().setValue(-300);
        root.getChildren().get(2).translateXProperty().setValue(300);
        root.getChildren().get(2).translateYProperty().setValue(0);
        
         scene = new Scene(root, 1500, 1200);
        //Scene tester = new Scene(test,100,100);
        //root.translateYProperty().setValue(100);
        //root.translateXProperty().setValue(100);
        primaryStage.setTitle("Regio Vinco Game");
        primaryStage.setScene(scene);
        primaryStage.show();
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
         docBuilderFactory = DocumentBuilderFactory.newInstance();
         docBuilder = docBuilderFactory.newDocumentBuilder();
         doc = docBuilder.parse (new File("H:/Homework6/src/newXMLDocument.xml"));
         docBuilder = docBuilderFactory.newDocumentBuilder();
         doc2 = docBuilder.parse (new File("H:\\Homework6\\src\\homework6\\Europe Data.xml"));
         
        NodeList listOfRegions = doc2.getElementsByTagName("sub_region");
        int totalRegion = listOfRegions.getLength();
       // NamedNodeMap attr = listOfRegions.item(0).getAttributes();
    //Node nodeAttr = attr.getNamedItem("fileID");
    //String node = nodeAttr.toString();
        reg = new ArrayList<Reg>();
        for (int i = 0;i <totalRegion;i++){
            NamedNodeMap attr = listOfRegions.item(i).getAttributes();
            Node nodeAttr = attr.getNamedItem("green");
            StringBuilder noder = new StringBuilder (nodeAttr.toString());
           
            noder.replace(0, 7, "");
            noder.delete(noder.length()-1, noder.length());
            String adder = noder.toString();
            int gren = Integer.parseInt(adder);
             nodeAttr = attr.getNamedItem("red");
             noder = new StringBuilder (nodeAttr.toString());
          
            noder.replace(0, 5, "");
            noder.delete(noder.length()-1, noder.length());
            adder = noder.toString();
            int rd = Integer.parseInt(adder);
            nodeAttr = attr.getNamedItem("blue");
             noder = new StringBuilder (nodeAttr.toString());
          
            noder.replace(0, 6, "");
            noder.delete(noder.length()-1, noder.length());
            adder = noder.toString();
            int blu = Integer.parseInt(adder);
            Color mapColor = Color.rgb(rd, gren, blu);
            nodeAttr = attr.getNamedItem("name");
             noder = new StringBuilder (nodeAttr.toString());
          
            noder.replace(0, 6, "");
            noder.delete(noder.length()-1, noder.length());
            String name = noder.toString();
            nodeAttr = attr.getNamedItem("capital");
             noder = new StringBuilder (nodeAttr.toString());
          
            noder.replace(0, 9, "");
            noder.delete(noder.length()-1, noder.length());
            String capital = noder.toString();
//            nodeAttr = attr.getNamedItem("leader");
//             noder = new StringBuilder (nodeAttr.toString());
          
//            noder.replace(0, 5, "");
//            noder.delete(noder.length()-1, noder.length());
//            String leader = noder.toString();
            Reg adding = new Reg();
            adding.Name = name;
            adding.Capital = capital;
            adding.Color = mapColor;
           // adding.Leader = leader;
            reg.add(adding);
            
            
            
            
             
            // System.out.println(noder.toString());
        }
         cr = new HashMap<Color, String>();
         cc = new HashMap<Color, String>();
         for ( int i = 0;i < reg.size();i++){
          
         cr.put(reg.get(i).Color ,reg.get(i).getName());
         cc.put(reg.get(i).Color ,reg.get(i).Capital);
         }
            
        
//        String filename = "H:/Homework6/src/newXMLDocument.xml";
//                builder = new StringBuilder();
//               FileReader reader = new FileReader (new File (filename));
//               int content ;
//               while ((content = reader.read())!= -1){
//                   builder.append((char)content);
//               }
//               reader.close();
//               System.out.println(builder.toString());
         docBuilder = docBuilderFactory.newDocumentBuilder();
         doc3 = docBuilder.parse (new File("H:\\Homework6\\src\\homework6\\Moldova Data.xml"));
         NodeList lmol = doc3.getElementsByTagName("sub_region");
         int tmol = lmol.getLength();
         mol = new ArrayList<Reg>();
        for (int i = 0;i <tmol;i++){
            NamedNodeMap attr = lmol.item(i).getAttributes();
            Node nodeAttr = attr.getNamedItem("green");
            StringBuilder noder = new StringBuilder (nodeAttr.toString());
           
            noder.replace(0, 7, "");
            noder.delete(noder.length()-1, noder.length());
            String adder = noder.toString();
            int gren = Integer.parseInt(adder);
             nodeAttr = attr.getNamedItem("red");
             noder = new StringBuilder (nodeAttr.toString());
          
            noder.replace(0, 5, "");
            noder.delete(noder.length()-1, noder.length());
            adder = noder.toString();
            int rd = Integer.parseInt(adder);
            nodeAttr = attr.getNamedItem("blue");
             noder = new StringBuilder (nodeAttr.toString());
          
            noder.replace(0, 6, "");
            noder.delete(noder.length()-1, noder.length());
            adder = noder.toString();
            int blu = Integer.parseInt(adder);
            Color mapColor = Color.rgb(rd, gren, blu);
            nodeAttr = attr.getNamedItem("name");
             noder = new StringBuilder (nodeAttr.toString());
          
            noder.replace(0, 6, "");
            noder.delete(noder.length()-1, noder.length());
            String name = noder.toString();
            nodeAttr = attr.getNamedItem("capital");
             noder = new StringBuilder (nodeAttr.toString());
          
            noder.replace(0, 9, "");
            noder.delete(noder.length()-1, noder.length());
            String capital = noder.toString();
//            nodeAttr = attr.getNamedItem("leader");
//             noder = new StringBuilder (nodeAttr.toString());
          
//            noder.replace(0, 5, "");
//            noder.delete(noder.length()-1, noder.length());
//            String leader = noder.toString();
            Reg adding = new Reg();
            adding.Name = name;
            adding.Capital = capital;
            adding.Color = mapColor;
           // adding.Leader = leader;
            mol.add(adding);
            
            
            
            
             
            // System.out.println(noder.toString());
        }
         cmr = new HashMap<Color, String>();
         cmc = new HashMap<Color, String>();
         for ( int i = 0;i < mol.size();i++){
          
         cmr.put(mol.get(i).Color ,mol.get(i).getName());
         cmc.put(mol.get(i).Color ,mol.get(i).Capital);
         }
         for (int i = 0;i<tmol;i++){
        System.out.println(mol.get(i).Capital);}
        launch(args);
        
        
    }
    public ImageView addGUIImage(Pane pane, String type, Image img, int x, int y) {	
	ImageView view = createImageView(img, x, y);
	pane.getChildren().add(view);
	guiImages.put(type, view);
	return view;
    }
    
    
    private ImageView createImageView(Image img, int x, int y) {
	ImageView view = new ImageView(img);
	view.setX(x);
	view.setY(y);
//        view.setFitWidth(100);
//        view.setPreserveRatio(true);
        
	return view;
    }
    private Image loadImage(String imagePath) {	
	Image img = new Image("file:" + imagePath);
	return img;
    }
   
    private void rEuro (){
         root.getChildren().remove(btn2);
            root.getChildren().remove(current);
            root.getChildren().remove(capital);
            root.getChildren().remove(clickedContinent);
            
}

}
